package route_test

import (
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/sj1k/route"
)

func ExampleErrorHandlerFunc_readme() {
    // error handler functions are rather straight forward, their signature just needs an error return value.
    errHandlerFn := func(w http.ResponseWriter, r *http.Request) error {
        if !r.URL.Query().Has("key") {
            return route.NewHTTPError(errors.New("key is a required param"), http.StatusBadRequest)
        }

        // do good stuffs
        w.WriteHeader(http.StatusOK)
        w.Write([]byte("ok"))
        return nil
    }

    // We must wrap the errHandlerFn to make it compatible with the standard routes.
    // ErrorHandlerFunc's by default are simple error responders, they will display the error via `http.Error`.
    wrapped := route.ErrorHandlerFunc(errHandlerFn)

    http.Handle("/", wrapped)
    http.ListenAndServe(":8080", nil)
}
