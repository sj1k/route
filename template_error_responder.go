package route

import (
	"bytes"
	"io"
	"net/http"

	"github.com/pkg/errors"
)


type Executor interface {
	Execute(w io.Writer, data any) error
}

type TemplateResponder struct {
	Template Executor

	DefaultErrorCode  int
}

func NewTemplateResponder(template Executor) TemplateResponder {
	return TemplateResponder{
		Template:          template,
		DefaultErrorCode:  http.StatusInternalServerError,
	}
}

func (responder TemplateResponder) ErrorResponse(w http.ResponseWriter, r *http.Request, err error) error {
	status := StatusCode(err, responder.DefaultErrorCode)
	httpError := NewHTTPError(err, status)

	var buff bytes.Buffer
	templateErr := responder.Template.Execute(&buff, httpError)

	if templateErr != nil {
		return errors.Wrap(templateErr, "failed to render response template")
	}

	w.WriteHeader(httpError.Status())
	w.Write(buff.Bytes())
	return nil
}
