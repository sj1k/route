package route_test

import (
	"log"
	"net/http"
	"os"

	"github.com/pkg/errors"
	"gitlab.com/sj1k/route"
)

func ExampleErrorResponderFunc_readme() {
	// This is a simple logger which logs the error, code and the file / line which created the error
	// and returns the error to also be displayed on the frontend (in this case by a fallback infallible responder)
	// The stack trace support is thanks to github.com/pkg/errors
	// If you use that package you already have traceback for your errors, the route package can pick up on it.
	customResponder := route.ErrorResponderFunc(func(w http.ResponseWriter, r *http.Request, err error) error {
		stackTrace := route.StackTrace(err)
		statusCode := route.StatusCode(err, http.StatusInternalServerError)

		frame := stackTrace[0]

		logger := log.New(os.Stderr, "", log.LstdFlags)

		// For more ways to format a frame.
		// see https://pkg.go.dev/github.com/pkg/errors#Frame.Format
		logger.Printf("%s %s [error %d] %s (%s:%d)\n", r.Method, r.URL, statusCode, err, frame, frame)
		return err
	})

	errHandlerFunc := route.ErrorHandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
		return route.NewHTTPError(
			errors.New("thingamajig not found"),
			http.StatusNotFound,
		)
	})

	handler := route.HandleErrors(customResponder, errHandlerFunc)

	http.Handle("/", handler)
	http.ListenAndServe(":3030", nil)
}
