package route

import "net/http"


type InfallibleResponder interface {
	InfallibleErrorResponse(w http.ResponseWriter, r *http.Request, err error)
}

type InfallibleResponderFunc func(w http.ResponseWriter, r *http.Request, err error)

func (infallible InfallibleResponderFunc) InfallibleErrorResponse(w http.ResponseWriter, r *http.Request, err error) {
	infallible(w, r, err)
}

func CreateFallback(responder ErrorResponder, infallible InfallibleResponder) InfallibleResponderFunc {
	return InfallibleResponderFunc(func(w http.ResponseWriter, r *http.Request, err error) {
		responderErr := responder.ErrorResponse(w, r, err)
		if responderErr != nil {
			infallible.InfallibleErrorResponse(w, r, err)
		}
	})
}
