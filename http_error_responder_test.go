package route_test

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"

	"github.com/pkg/errors"
	"gitlab.com/sj1k/route"
)


func ExampleErrorHandlerFunc() {
	// error handler functions are rather straight forward, their signature just needs an error return value.
	errHandlerFn := func(w http.ResponseWriter, r *http.Request) error {
		if !r.URL.Query().Has("key") {
			return route.NewHTTPError(errors.New("key is a required param"), http.StatusBadRequest)
		}

		// do good stuffs
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("ok"))
		return nil
	}

	// We must wrap the errHandlerFn to make it compatible with the standard routes.
 	// ErrorHandlerFunc's by default are simple error handlers, they will display the error via `http.Error`.
	wrapped := route.ErrorHandlerFunc(errHandlerFn)

	// Usually the wrapped route would go into something like
	// `http.Handle("/blah", wrapped)`
	server := httptest.NewServer(wrapped)
	defer server.Close()


	// Request with the required key.
	url, _ := url.Parse(fmt.Sprintf("%s/something?key=cool", server.URL)) 
	resp, _ := http.Get(url.String())

	body, _ := io.ReadAll(resp.Body)
	defer resp.Body.Close()
	fmt.Println("status:", resp.StatusCode, string(body))

	// Request without the required key, this should produce an HTTP error.
	url, _ = url.Parse(fmt.Sprintf("%s/something?notkey=notcool", server.URL))
	respErr, _ := http.Get(url.String())

	body, _ = io.ReadAll(respErr.Body)
	defer respErr.Body.Close()
	fmt.Println("status:", respErr.StatusCode, string(body))

	// `http.Error` produces plain text errors.
	// This is useful for text based endpoints and also an early dev / testing stuff.

	// Output:
	// status: 200 ok
	// status: 400 key is a required param
}
