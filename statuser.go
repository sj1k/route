package route

import "github.com/pkg/errors"

type Statuser interface {
	Status() int
}

func StatusCode(err error, defaultCode int) int {
	var status int = defaultCode;
	var statuser Statuser
	if errors.As(err, &statuser) {
		status = statuser.Status()
	}
	return status
}

