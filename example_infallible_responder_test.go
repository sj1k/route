package route_test

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/pkg/errors"
	"gitlab.com/sj1k/route"
)

func ExampleInfallibleResponder_readme() {
	// Fallbacks cannot bubble errors further and should handle the error somehow.
	fallbackResponder := route.InfallibleResponderFunc(func(w http.ResponseWriter, r *http.Request, err error) {
		statusCode := route.StatusCode(err, http.StatusInternalServerError)

		// Respond with a similar format to the logger but without any stack trace.
		http.Error(w, fmt.Sprintf("[error %d] %s", statusCode, err), statusCode)
	})

	customResponder := route.ErrorResponderFunc(func(w http.ResponseWriter, r *http.Request, err error) error {
		stackTrace := route.StackTrace(err)
		statusCode := route.StatusCode(err, http.StatusInternalServerError)
		frame := stackTrace[0]
		logger := log.New(os.Stderr, "", log.LstdFlags)
		logger.Printf("%s %s [error %d] %s (%s:%d)\n", r.Method, r.URL, statusCode, err, frame, frame)
		return err
	})

	errHandlerFunc := route.ErrorHandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
		return route.NewHTTPError(
			errors.New("thingamajig not found"),
			http.StatusNotFound,
		)
	})

	handler := route.HandleErrors(customResponder, errHandlerFunc).WithFallback(fallbackResponder)

	http.Handle("/", handler)
	http.ListenAndServe(":3030", nil)
}
