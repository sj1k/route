# route

[![Gitlab Pipeline Status](https://img.shields.io/gitlab/pipeline-status/sj1k%2Froute?branch=master&style=flat-square)](https://gitlab.com/sj1k/route/-/pipelines)
[![Gitlab Code Coverage](https://img.shields.io/gitlab/pipeline-coverage/sj1k%2Froute?branch=master&style=flat-square)](https://gitlab.com/sj1k/route/-/pipelines)
[![Godoc Reference](https://img.shields.io/badge/godoc-reference-blue?style=flat-square)](https://pkg.go.dev/gitlab.com/sj1k/route)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/sj1k/route?style=flat-square)](https://goreportcard.com/report/gitlab.com/sj1k/route)
[![LICENSE](https://img.shields.io/gitlab/license/sj1k/route?style=flat-square)](https://gitlab.com/sj1k/route/-/blob/main/LICENSE)

---

A lightweight package for easily displaying / handling errors (optionally with [stack trace](#custom-responders)) from http handlers.
Simply return your errors, if they have status codes they will be used. (see [statuser interface](https://pkg.go.dev/gitlab.com/sj1k/route#Statuser))

```go
errHandlerFunc := route.ErrorHandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
        return route.NewHTTPError(
                errors.New("thingamajig not found"),
                http.StatusNotFound,
        )
})
```

Built in handlers with `http.Error` or text / html template handlers.

Designed to be used with: 

- [std lib net/http](https://pkg.go.dev/net/http)
- [std lib html/template or text/template](https://pkg.go.dev/html/template)
- [github.com/pkg/errors](https://pkg.go.dev/github.com/pkg/errors)


# Contents


- [Basic ErrorHandlerFunc](#errorhandlerfunc)
- [Custom Responders](#custom-responders):
	- [Logging](#logging)
	- [Render errors to templates with TemplateResponder](#template-responder)
- [Catch all unhandled errors with InfallibleResponder](#infallible-responders)


# ErrorHandlerFunc


The simplest way to use this package is with [`route.ErrorHandlerFunc`](https://pkg.go.dev/gitlab.com/sj1k/route#ErrorHandlerFunc). This provides a quick way to get errors at least surfaced.

```go
import (
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/sj1k/route"
)

func ExampleErrorHandlerFunc_readme() {
    // error handler functions are rather straight forward, their signature just needs an error return value.
    errHandlerFn := func(w http.ResponseWriter, r *http.Request) error {
        if !r.URL.Query().Has("key") {
            // You can return any error which satisfies the `route.Statuser` interface
            // and this package will pick up on the correct status for the errors.
            // This package provides a simple `HTTPError` which supports a status.
            return route.NewHTTPError(errors.New("key is a required param"), http.StatusBadRequest)
        }

        // do good stuffs
        w.WriteHeader(http.StatusOK)
        w.Write([]byte("ok"))
        return nil
    }

    // We must wrap the errHandlerFn to make it compatible with the standard routes.
    // ErrorHandlerFunc's by default are simple error responders, they will display the error via `http.Error`.
    wrapped := route.ErrorHandlerFunc(errHandlerFn)

    http.Handle("/", wrapped)
    http.ListenAndServe(":8080", nil)
}
```

ErrorHandlerFuncs are simple and self contained but limited.

This package provides another [template responder](#template-responder) and can use [custom responders](#custom-responders).


# Custom responders

## Logging

Custom error responders can be made easily via the [`route.ErrorResponderFunc`](https://pkg.go.dev/gitlab.com/sj1k/route#ErrorResponderFunc) wrapper.


```go
import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/pkg/errors"
	"gitlab.com/sj1k/route"
)

func ExampleCustomResponder_readme() {
	// This is a simple logger which logs the error, code and the file / line which created the error
	// and returns the error to also be displayed on the frontend (in this case by a fallback infallible responder)
	// The stack trace support is thanks to github.com/pkg/errors
	// If you use that package you already have traceback for your errors, the route package can pick up on it.
	customResponder := route.ErrorResponderFunc(func(w http.ResponseWriter, r *http.Request, err error) error {
		stackTrace := route.StackTrace(err)
		statusCode := route.StatusCode(err, http.StatusInternalServerError)

		frame := stackTrace[0]

		logger := log.New(os.Stderr, "", log.LstdFlags)

		// For more ways to format a frame.
		// see https://pkg.go.dev/github.com/pkg/errors#Frame.Format
		logger.Printf("%s %s [error %d] %s (%s:%d)\n", r.Method, r.URL, statusCode, err, frame, frame)
		return err
	})

	errHandlerFunc := route.ErrorHandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
		return route.NewHTTPError(
			errors.New("thingamajig not found"),
			http.StatusNotFound,
		)
	})

	handler := route.HandleErrors(customResponder, errHandlerFunc)

	http.Handle("/", handler)
	http.ListenAndServe(":3030", nil)
}
```

```
2024/05/30 10:14:08 GET / [error 404] thingamajig not found (example_custom_responder_test.go:33)
```

## Template responder

To respond with templates we can use the [`route.TemplateResponder`](https://pkg.go.dev/gitlab.com/sj1k/route#TemplateResponder).


```go
import (
	"html/template"
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/sj1k/route"
)

func ExampleTemplateResponder_readme() {
	errTemplate := template.Must(template.New("error").Parse(
		`<p style="color: red;"><span style="font-weight: bold;">{{ .StatusCode }}:</span> {{ .Err }}</p>`,
	))
	errTemplateResponder := route.NewTemplateResponder(errTemplate)

	errHandlerFunc := route.ErrorHandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
		return route.NewHTTPError(
			errors.New("thingamajig not found"),
			http.StatusNotFound,
		)
	})

	// HandleErrors provides a way to take any ErrorHandler's and tie a ErrorResponder to them. 
	// This will attempt to handle the error via the ErrorResponder and if THAT also fails or bubbles the error it will use a fallback.
	// (see `route.InfallibleResponder`)
	handler := route.HandleErrors(errTemplateResponder, errHandlerFunc)

	http.Handle("/", handler)
	http.ListenAndServe(":3030", nil)
}
```

Both `text/template` and `html/template` are supported.
This can be any template engine which satisfies the `route.Executor` interface. 



# Infallible responders


```go
import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/pkg/errors"
	"gitlab.com/sj1k/route"
)

func ExampleInfallibleResponder_readme() {
	// Fallbacks cannot bubble errors further and should handle the error somehow.
	fallbackResponder := route.InfallibleResponderFunc(func(w http.ResponseWriter, r *http.Request, err error) {
		statusCode := route.StatusCode(err, http.StatusInternalServerError)

		// Respond with a similar format to the logger but without any stack trace.
		http.Error(w, fmt.Sprintf("[error %d] %s", statusCode, err), statusCode)
	})

	customResponder := route.ErrorResponderFunc(func(w http.ResponseWriter, r *http.Request, err error) error {
		stackTrace := route.StackTrace(err)
		statusCode := route.StatusCode(err, http.StatusInternalServerError)
		frame := stackTrace[0]
		logger := log.New(os.Stderr, "", log.LstdFlags)
		logger.Printf("%s %s [error %d] %s (%s:%d)\n", r.Method, r.URL, statusCode, err, frame, frame)
		return err
	})

	errHandlerFunc := route.ErrorHandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
		return route.NewHTTPError(
			errors.New("thingamajig not found"),
			http.StatusNotFound,
		)
	})

	handler := route.HandleErrors(customResponder, errHandlerFunc).WithFallback(fallbackResponder)

	http.Handle("/", handler)
	http.ListenAndServe(":3030", nil)
}
```
