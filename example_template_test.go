package route_test

import (
	"html/template"
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/sj1k/route"
)

func ExampleTemplateResponder_readme() {
	errTemplate := template.Must(template.New("error").Parse(
		`<p style="color: red;"><span style="font-weight: bold;">{{ .StatusCode }}:</span> {{ .Err }}</p>`,
	))
	errTemplateResponder := route.NewTemplateResponder(errTemplate)

	errHandlerFunc := route.ErrorHandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
		// For this example it will just always fail. We don't know what this thingamajig is :>
		return route.NewHTTPError(
			errors.New("thingamajig not found"),
			http.StatusNotFound,
		)
	})

	// HandleErrors provides a way to take any ErrorHandler's and tie a ErrorResponder to them. 
	// This will attempt to handle the error via the ErrorResponder and if THAT also fails or bubbles the error it will use a fallback.
	// (see `route.InfallibleResponder`)
	handler := route.HandleErrors(errTemplateResponder, errHandlerFunc)

	http.Handle("/", handler)
	http.ListenAndServe(":3030", nil)
}
