package route

import (
	"net/http"
)

type ErrorResponder interface {
	ErrorResponse(w http.ResponseWriter, r *http.Request, err error) error
}

type ErrorResponderFunc func(w http.ResponseWriter, r *http.Request, err error) error

func (responder ErrorResponderFunc) ErrorResponse(w http.ResponseWriter, r *http.Request, err error) error {
	return responder(w, r, err)
}
