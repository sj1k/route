package route


// HTTPError is a simple Status aware error wrapper.
// These can be used to return status aware errors from `ErrorHandler`s.
// This is also the error type which gets passed to the `TemplateResponder`.
type HTTPError struct {
	Err        error
	StatusCode int
}

func NewHTTPError(err error, status int) HTTPError {
	return HTTPError{
		Err:  err,
		StatusCode: status,
	}
}

// Unwrap the original error. 
// This allows the error to work with stack trace / unwrapping / etc via `https://pkg.go.dev/github.com/pkg/errors`
func (httpErr HTTPError) Unwrap() error {
	return httpErr.Err
}

func (h HTTPError) Error() string {
	return h.Err.Error()
}

func (h HTTPError) Status() int {
	return h.StatusCode
}
