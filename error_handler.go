package route

import (
	"net/http"
)

// ErrorHandler is any type which can return errors from a HTTP handler.
type ErrorHandler interface {
	ServeHTTPError(http.ResponseWriter, *http.Request) error
}

// ErrorHandlerFunc is a simple wrapper around a HTTP handler func which can return errors.
// These have a built in infallible fallback so any errors returned from these will be handled.
type ErrorHandlerFunc func(http.ResponseWriter, *http.Request) error

func (handler ErrorHandlerFunc) ServeHTTPError(w http.ResponseWriter, r *http.Request) error {
	return handler(w, r)
}

// ServeHTTP renders any errors from the ErrorHandlerFunc with an infallible fallback handler.
// This ensures ErrorHandlerFuncs are always handled somehow.
func (handler ErrorHandlerFunc) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	err := handler.ServeHTTPError(w, r)
	if err != nil {
		InfallibleHTTPErrorFunc(w, r, err)
	}
}
