package route

import (
	"net/http"
)

func HTTPErrorResponder() ErrorResponderFunc {
	return ErrorResponderFunc(HTTPErrorFunc)
}

func HTTPErrorFunc(w http.ResponseWriter, r *http.Request, err error) error {
	InfallibleHTTPErrorFunc(w, r, err)
	return nil
}

func InfallibleHTTPErrorFunc(w http.ResponseWriter, r *http.Request, err error) {
	code := StatusCode(err, http.StatusInternalServerError)
	http.Error(w, err.Error(), code)
}
