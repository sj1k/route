package route

import (
	"github.com/pkg/errors"
)

type StackTracer interface {
	StackTrace() errors.StackTrace
}

func StackTrace(err error) errors.StackTrace {
	var stackTrace errors.StackTrace
	var tracer StackTracer
	if errors.As(err, &tracer) {
		stackTrace = tracer.StackTrace()
	}
	return stackTrace
}
