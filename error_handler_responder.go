package route

import "net/http"

type ErrorHandlerResponder struct {
	Next      ErrorHandler
	Responder ErrorResponder
	Fallback  InfallibleResponder
}

func HandleErrors(responder ErrorResponder, next ErrorHandler) ErrorHandlerResponder {
	return ErrorHandlerResponder{
		Next:      next,
		Responder: responder,
		Fallback:  InfallibleResponderFunc(InfallibleHTTPErrorFunc),
	}
}

func (handler ErrorHandlerResponder) ErrorResponse(w http.ResponseWriter, r *http.Request, err error) error {
	return handler.Responder.ErrorResponse(w, r, err)
}

func (handler ErrorHandlerResponder) WithFallback(fallback InfallibleResponder) ErrorHandlerResponder {
	handler.Fallback = fallback
	return handler
}

func (handler ErrorHandlerResponder) ServeHTTPError(w http.ResponseWriter, r *http.Request) error {
	err := handler.Next.ServeHTTPError(w, r)
	if err != nil {
		return handler.ErrorResponse(w, r, err)
	}
	return nil
}

func (handler ErrorHandlerResponder) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	err := handler.ServeHTTPError(w, r)
	if err != nil {
		handler.Fallback.InfallibleErrorResponse(w, r, err)
	}
}
